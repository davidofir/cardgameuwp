﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    public struct GameScore
    {
        //Declare the variable that represents the score
        private int _userScore;
        private int _houseScore;

    }
    class CardGame 
    {
       //Declare the card deck
        private CardDeck _cardDeck;
        //Declare the score for the game
        private GameScore _score;
        //Declare the current card played by the house
        private Card _houseCard;
        //Declare the current card player by the name
        private Card _playerCard;

    }
}
